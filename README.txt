As per the norm after the Git migration,
the XRDS Simple code now lives in one of the two version specific branches:

6.x-1.x - Drupal 6
7.x-1.x - Drupal 7

Please checkout one of those in order to find the code
